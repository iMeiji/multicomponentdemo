package com.meiji.commonbase;

public class RouterConstant {

    public static final String MODULE_HOME = "/modulehome/";
    public static final String MODULE_DASH = "/moduledash/";


    public static final String toHomeMainActivity = MODULE_HOME + "HomeMainActivity";
    public static final String toHomeProvider = MODULE_HOME + "HomeProvider";
    public static final String toHomeFragment = MODULE_HOME + "HomeFragment";

    public static final String toDashFragment = MODULE_DASH + "DashFragment";
}
